# Conversión a audio de la carta de Alfonsina Storni.

### 1)Investigué varias páginas hasta que me encontré con TTSMAKER.

### 2)TTSMAKER  es un convertidor de texto a voz totalmente gratuito y te otorga los derechos de autor.

### 3)En la sección de "idioma" seleccioné "Español(Sapnish)" la opción "330022-Elena-ar Argentina Female Voice"

### 4)Ésta opción contiene un límite de 6000 caracteres.

### 5)Luego copié la carta de Alfonsina y seleccioné  el botón amarillo: "iniciar la conversión".

### 6)Lo escuché para verificar que el audio sea claro, correcto, y lo descargué en formato MP3.